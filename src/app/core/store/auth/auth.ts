export interface Auth {
  token: string;
  displayName: string;
  role: 'admin' | 'moderator';
}

export interface Credentials {
  email: string;
  password: string;
}
