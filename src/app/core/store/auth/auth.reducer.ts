import { Auth } from './auth';
import { createReducer, on } from '@ngrx/store';
import * as LoginActions from './auth.actions';

export interface AuthState {
  auth: Auth;
  error: boolean;
  profile?: any;
}

export const initialState: AuthState = {
  auth: null,
  error: false,
};

export const authReducer = createReducer(
  initialState,
  on(LoginActions.saveFromLocalStorage, (state, action) => ({  auth: action.auth, error: false })),
  on(LoginActions.loginSuccess, (state, action) => ({  auth: action.auth, error: false })),
  on(LoginActions.loginFailed, (state) => ({  ...state, error: true })),
  on(LoginActions.logout, () => ({  ...initialState  })),
);

