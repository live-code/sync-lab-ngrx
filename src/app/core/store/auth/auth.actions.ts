import { createAction, props } from '@ngrx/store';
import { Auth } from './auth';


export const login = createAction(
  '[Auth] login',
  props<{ email: string, password: string }>()
);

export const loginSuccess = createAction(
  '[Auth] login success',
  props<{ auth: Auth }>()
);

export const loginFailed = createAction(
  '[Auth] login failed'
);

export const logout = createAction(
  '[Auth] logout'
);


export const saveFromLocalStorage = createAction(
  '[Init] save from localstorage',
  props<{ auth: Auth }>()
);
