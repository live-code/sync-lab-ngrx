import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { iif, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getIsLogged } from './auth.selectors';
import { tap } from 'rxjs/operators';
import { go } from '../router/router.actions';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(private store: Store) { }

  canActivate(): Observable<boolean> {
    return this.store.select(getIsLogged)
      .pipe(
        tap(isLogged => {
          if (!isLogged) {
            this.store.dispatch(go({ path: '/login' }));
          }
        }),
      );
  }
}
