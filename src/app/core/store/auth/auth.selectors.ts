import { AppState } from '../../../app.module';
import { createSelector } from '@ngrx/store';

export const getAuth = (state: AppState) => state.authentication.auth;
export const getAuthError = (state: AppState) => state.authentication.error;

export const getIsLogged = createSelector(getAuth, state => !!state);
export const getToken = createSelector(getAuth, state => state?.token);
export const getRole = createSelector(getAuth, state => state?.role);
export const getDisplayName = createSelector(getAuth, state => state?.displayName);


