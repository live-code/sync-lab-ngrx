import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { iif, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getIsLogged, getRole } from './auth.selectors';
import { map, tap, withLatestFrom } from 'rxjs/operators';
import { go } from '../router/router.actions';

@Injectable({ providedIn: 'root' })
export class AdminGuard implements CanActivate {

  constructor(private store: Store) { }

 /* canActivate(): Observable<boolean> {
    return this.store.select(getRole)
      .pipe(
        map(role => role === 'admin'),
        tap(isAllowed => {
          if (!isAllowed) {
            this.store.dispatch(go({ path: '/login' }));
          }
        }),
      );
  }*/

  canActivate(): Observable<boolean> {
    return this.store.select(getIsLogged)
      .pipe(
        withLatestFrom(this.store.select(getRole)),
        map(([isLogged, role]) => {
          if (role !== 'admin') {
            this.store.dispatch(go({ path: '/login' }));
          }
          return isLogged;
        }),
      );
  }
}
