import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { iif, Observable, of, throwError } from 'rxjs';
import { Store } from '@ngrx/store';
import { getIsLogged, getRole, getToken } from './auth.selectors';
import { catchError, first, mergeMap, withLatestFrom } from 'rxjs/operators';
import { go } from '../router/router.actions';
import { logout } from './auth.actions';

@Injectable({ providedIn: 'root' })
export class AuthInterceptor implements HttpInterceptor {
  constructor(private store: Store) { }

 /* intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('interceptor')
    return this.store.select(getToken)
      .pipe(
        first(),
        mergeMap(token => {
          const request = !!token && req.url.includes('localhost') ? req.clone({
            setHeaders:  { Authorization: 'bearer ' + token}
          }) : req;
          return next.handle(request);
        })
      );
  }*/

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select(getIsLogged)
      .pipe(
        first(),
        withLatestFrom(this.store.select(getToken)),
        mergeMap(([isLogged, token]) => {
          return iif(
            () => isLogged && req.url.includes('localhost'),
            next.handle(req.clone({ setHeaders:  { Authorization: 'bearer ' + token} })),
            next.handle(req)
          );
        }),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 404:
                console.log('errore!!!');
                break;
              default:
              case 401:
                // this.store.dispatch(go({ path: ''}));
                this.store.dispatch(logout());
                break;
            }
          }
          return throwError(err);
        })
      );
  }
}
