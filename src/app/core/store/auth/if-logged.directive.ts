import { Directive, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.module';
import { getIsLogged } from './auth.selectors';
import { distinctUntilChanged, filter, takeUntil, tap } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';

@Directive({
  selector: '[slIfLogged]'
})
export class IfLoggedDirective implements OnDestroy {

  // sub: Subscription;
  private destroy$ = new Subject();

  constructor(
    private view: ViewContainerRef,
    private tpl: TemplateRef<any>,
    private store: Store<AppState>
  ) {
    this.store.select(getIsLogged)
      .pipe(
        tap(() => view.clear()),
        distinctUntilChanged(),
        filter(isLogged => !!isLogged),
        takeUntil(this.destroy$)
      )
      .subscribe(() => view.createEmbeddedView(tpl));

  }

  ngOnDestroy(): void {
    // this.sub.unsubscribe();
    this.destroy$.next();
    this.destroy$.complete();
  }

}
