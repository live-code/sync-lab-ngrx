import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as LoginActions from './auth.actions';
import { catchError, concatMap, delay, exhaustMap, filter, map, mapTo, mergeMap, switchMap, tap } from 'rxjs/operators';
import { Auth } from './auth';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import * as RouterActions from '../router/router.actions';

@Injectable()
export class AuthEffects {

  init$ = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    mapTo(JSON.parse(localStorage.getItem('auth')) as Auth),
    filter(auth => !!auth),
    map(auth => LoginActions.saveFromLocalStorage({ auth }))
  ));

  login$ = createEffect(() => this.actions$.pipe(
    ofType(LoginActions.login),
    exhaustMap(action => {
      const params: HttpParams = new HttpParams()
        .set('email', action.email)
        .set('pass', action.password);

      return this.http.get<Auth>(`http://localhost:3000/login`, { params })
        .pipe(
          map(auth => LoginActions.loginSuccess({ auth })),
          catchError(() => of(LoginActions.loginFailed()))
        );
    })
  ));

  loginSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(LoginActions.loginSuccess),
    tap(({ auth }) => localStorage.setItem('auth', JSON.stringify(auth))),
    mapTo(RouterActions.go({ path: 'admin'}))
  ));

  logout$ = createEffect(() => this.actions$.pipe(
    ofType(LoginActions.logout),
    tap(() => localStorage.removeItem('auth')),
    mapTo(RouterActions.go({ path: '/'}))
  ));

  constructor(
    private actions$: Actions,
    private http: HttpClient,
  ) {
  }
}
