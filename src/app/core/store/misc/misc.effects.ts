import { Injectable } from '@angular/core';
import { Actions, createEffect } from '@ngrx/effects';
import { fromEvent } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';

@Injectable()
export class MiscEffects {

  resize$ = createEffect(() => fromEvent(window, 'resize').pipe(
    debounceTime(1000),
    tap(console.log),
    // map(e => resize({ width: e....}))
  ), { dispatch: false} );

}
