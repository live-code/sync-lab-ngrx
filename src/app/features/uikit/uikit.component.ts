import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'sl-uikit',
  template: `

    <div class="row">
      <div class="col-sm-6">1</div>
      <div class="col-sm-3">2</div>
      <div class="col-sm-3">3</div>
    </div>
    
    <div slRow="sm">
      <div [slCol]="6">1</div>
      <div slCol="3">2</div>
      <div slCol="3">3</div>
    </div>

    <div slCol="5">5</div>
    
    Ciao, vai su <span slUrl="http://www.google.com">google</span>

    <button slUrl="http://www.adobe.com">adobe</button>
    <hr>
    <h1 [slBg]="color" 
        [fontSize]="font">Hello</h1>
    
    <button (click)="color = 'purple'">purple</button>
    <button (click)="color = 'red'">red</button>
    <button (click)="font = '30'">30</button>
    <button (click)="font = '100'">100</button>
  `,
})
export class UikitComponent  {
  color = 'purple';
  font = '100';

  constructor() {
  }
  // @ViewChild() host: ElementRef<HTMLDivElement>

}
