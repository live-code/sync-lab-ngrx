import { createReducer, on } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';
import * as PexelsSearchActions from './pexels-video-search.actions';

export interface PexelsVideoSearchState {
  list: Video[];
  hasError: boolean;
  isPending: boolean;
}

const initialState = {
  list: [],
  hasError: false,
  isPending: false
};

export const searchPexelsVideoReducer = createReducer(
  initialState,
  on(PexelsSearchActions.searchVideo, (state, action) => ({ ...state, hasError: false, isPending: true })),
  on(PexelsSearchActions.searchVideoSuccess, (state, action) => ({ ...state, list: action.items, isPending: false })),
  on(PexelsSearchActions.searchVideoFail, (state, action) => ({ ...state, isPending: false, hasError: true })),
);
