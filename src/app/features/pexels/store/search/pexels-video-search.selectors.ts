import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoState } from '../../pexels.module';
import { getPexelsCurrentVideo } from '../video-player/player.selectors';

export const getPexelsFeature =
  createFeatureSelector<PexelsVideoState>('pexels');

export const getPexelsVideo = createSelector(
  getPexelsFeature,
  state => state.search.list.filter(item => item.width >= state.filter.minResolution)
);

export const getPexelsVideoError = createSelector(
  getPexelsFeature,
  state => state.search.hasError
);

export const getPexelsVideoPending = createSelector(
  getPexelsFeature,
  state => state.search.isPending
);

export const getPexelsVideoPictures = createSelector(
  getPexelsVideo,
  getPexelsCurrentVideo,
  (videos, currentVideo) => videos.find(v => v.id === currentVideo?.id)?.video_pictures
    .map(v => v.picture)
)
