import { createAction, props } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';

export const searchVideo = createAction(
  '[Pexels Video] Search',
  props<{ text: string }>()
);

export const searchVideoSuccess = createAction(
  '[Pexels Video] Search Success',
  props<{ items: Video[]}>()
);

export const searchVideoFail = createAction(
  '[Pexels Video] Search Fail',
);


