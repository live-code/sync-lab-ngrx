import { HttpClient } from '@angular/common/http';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { searchVideo, searchVideoFail, searchVideoSuccess } from './pexels-video-search.actions';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { PexelsVideoResponse } from '../../model/pexels-video-response';
import { of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class PexelsVideoSearchEffects {

  searchVideo$ = createEffect(() => this.actions$.pipe(
    ofType(searchVideo),
    mergeMap(
      action => this.http.get<PexelsVideoResponse>(`https://api.pexels.com/videos/search?per_page=15&page=1&query=${action.text}`, {
        headers: { Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be'}
      })
        .pipe(
          map(result => searchVideoSuccess({ items: result.videos})),
          catchError(() => of(searchVideoFail()))
        )
    )
  ));

  constructor(
    private http: HttpClient,
    private actions$: Actions
  ) { }
}
