import { createAction, props } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';

export const showVideo = createAction(
  '[Pexels Player] Show',
  props<{ video: Video }>()
)
