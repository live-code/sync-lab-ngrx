import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoState } from '../../pexels.module';

export const getPexelsFeature =
  createFeatureSelector<PexelsVideoState>('pexels');


export const getPexelsCurrentVideo = createSelector(
  getPexelsFeature,
  state => state.player.currentVideo
);

export const getPexelsCurrentVideoUrl = createSelector(
  getPexelsCurrentVideo,
  state => state?.video_files[0].link
);
