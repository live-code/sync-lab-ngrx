import { Video } from '../../model/pexels-video-response';
import { createReducer, on } from '@ngrx/store';
import { showVideo } from './player.actions';
import { searchVideo } from '../search/pexels-video-search.actions';

export interface PexelsPlayerState {
  currentVideo: Video;
}

export const initialState: PexelsPlayerState = {
  currentVideo: null
};

export const playerReducer = createReducer(
  initialState,
  on(searchVideo, () => ({ currentVideo: null })),
  on(showVideo, (state, action) => ({ currentVideo: action.video}))
);
