import { createAction, props } from '@ngrx/store';

export const filterByMinResolution = createAction(
  '[Pexels Video] filter by min resolution',
  props<{ minResolution: number }>()
);
