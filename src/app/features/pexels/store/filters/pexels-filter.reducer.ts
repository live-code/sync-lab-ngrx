import { createReducer, on } from '@ngrx/store';
import { filterByMinResolution } from './pexels-filter.actions';
import { searchVideo } from '../search/pexels-video-search.actions';

export interface PexelsFilterState {
  minResolution: number;
  // ...
  // ...
  // ...
}

export const initialState: PexelsFilterState = {
  minResolution: 0,
  // ...
};

export const filterReducer = createReducer(
  initialState,
  on(searchVideo, () => ({...initialState})),
  on(filterByMinResolution, (state, action) => ({...state, minResolution: action.minResolution}))
);
