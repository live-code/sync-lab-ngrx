import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoState } from '../../pexels.module';

export const getPexelsFeature =
  createFeatureSelector<PexelsVideoState>('pexels');

export const getFilterResolution = createSelector(
  getPexelsFeature,
  state => state.filter.minResolution
);
