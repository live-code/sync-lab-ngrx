import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PexelsRoutingModule } from './pexels-routing.module';
import { PexelsComponent } from './pexels.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { PexelsVideoSearchState, searchPexelsVideoReducer } from './store/search/pexels-video-search.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PexelsVideoSearchEffects } from './store/search/pexels-video-search.effects';
import { PexelsPlayerState, playerReducer } from './store/video-player/player.reducer';
import { filterReducer, PexelsFilterState } from './store/filters/pexels-filter.reducer';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

export interface PexelsVideoState {
  search: PexelsVideoSearchState;
  player: PexelsPlayerState;
  filter: PexelsFilterState;
}

const reducers: ActionReducerMap<PexelsVideoState> = {
  search: searchPexelsVideoReducer,
  player: playerReducer,
  filter: filterReducer
};


@NgModule({
  declarations: [PexelsComponent],
  imports: [
    CommonModule,
    PexelsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature('pexels', reducers),
    EffectsModule.forFeature([ PexelsVideoSearchEffects ])
  ]
})
export class PexelsModule { }
