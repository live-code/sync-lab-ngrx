import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { searchVideo } from './store/search/pexels-video-search.actions';
import { Observable } from 'rxjs';
import { Video } from './model/pexels-video-response';
import { getPexelsVideo, getPexelsVideoPending, getPexelsVideoPictures } from './store/search/pexels-video-search.selectors';
import { showVideo } from './store/video-player/player.actions';
import { getPexelsCurrentVideoUrl } from './store/video-player/player.selectors';
import { filterByMinResolution } from './store/filters/pexels-filter.actions';
import { getFilterResolution } from './store/filters/pexels-filter.selectors';

@Component({
  selector: 'sl-pexels',
  template: `    
    <!--video player-->
    <video
      controls
      *ngIf="video$ | async as video"
      [src]="video" width="100%">
      Your browser does not support videos
    </video>
    
    <div *ngIf="videoPending$ | async">loading...</div>
    
    <!--search-->
    <input type="text"
           placeholder="Search Something..."
           (keydown.enter)="searchHandler($event)">
    <br>
    
    <!--active video pictures-->
    <img
      *ngFor="let img of videoPictures$ | async"
      [src]="img" alt=""
      width="30"
    >
    
    <!--filters-->
    <div>
      <select (change)="filterByResHandler($event)" [ngModel]="minRes$ | async" name="minRes">
        <option [value]="0">0</option>
        <option [value]="1920">1920</option>
        <option [value]="3000">3000</option>
        <option [value]="3840">3840</option>    
      </select>
    </div>
    
    <!--search result-->
    <div 
      *ngFor="let video of videos$ | async" 
      style="display: flex"
      (click)="showVideoHandler(video)"
    >
      <img [src]="video.image" width="60">
      <div>{{video.width}} x {{video.height}} px</div>
    </div>
  `,
})
export class PexelsComponent {
  videos$: Observable<Video[]> = this.store.select(getPexelsVideo);
  videoPending$: Observable<boolean> = this.store.select(getPexelsVideoPending);
  videoPictures$: Observable<string[]> = this.store.select(getPexelsVideoPictures);
  video$: Observable<string> = this.store.select(getPexelsCurrentVideoUrl);
  minRes$: Observable<number> = this.store.select(getFilterResolution)

  constructor(private store: Store) {
    store.dispatch(searchVideo({ text: 'animals' }));
  }

  searchHandler(event: KeyboardEvent): void {
    const input: HTMLInputElement = event.target as HTMLInputElement;
    this.store.dispatch(searchVideo({ text: input.value }));
  }

  showVideoHandler(video: Video): void {
    this.store.dispatch(showVideo({ video }));
  }

  filterByResHandler(event: Event): void {
    const minRes = (event.target as HTMLSelectElement).value;
    this.store.dispatch(filterByMinResolution({ minResolution: +minRes}))
  }
}
