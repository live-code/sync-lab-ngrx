import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { go } from '../../core/store/router/router.actions';
import { login } from '../../core/store/auth/auth.actions';
import { Observable } from 'rxjs';
import { getAuthError } from '../../core/store/auth/auth.selectors';

@Component({
  selector: 'sl-login',
  template: `
    
    <div *ngIf="authError$ | async">c'è un errore</div>
    <h1>Login</h1>
    
    <button (click)="loginHandler()">SignIn</button>
    
  `,
  styles: [
  ]
})
export class LoginComponent {
  authError$: Observable<boolean> = this.store.select(getAuthError);

  constructor(private store: Store<AppState>) {}

  loginHandler(): void {
    this.store.dispatch(login({ email: 'a', password: 'b'}))
  }
}
