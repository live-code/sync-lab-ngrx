import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'sl-admin',
  template: `
    <p>
      admin works!
    </p>
  `,
  styles: [
  ]
})
export class AdminComponent implements OnInit {

  constructor(private http: HttpClient) {
    http.get('http://localhost:3000/items')
      .subscribe(
        val => console.log(val),
        err => console.log('ERROR ACTION', err)
      );
  }

  ngOnInit(): void {
  }

}
