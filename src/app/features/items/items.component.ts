import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { fromEvent, Observable } from 'rxjs';
import { Item } from '../../model/item';
import { getItems, getItemsError } from './store/items.selectors';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { addItem, addItemSuccess, deleteItem, loadItems } from './store/items.actions';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'sl-items',
  template: `

    <div *ngIf="getItemsError$ | async as itemError">
      {{itemError}}
    </div>

    <form #f="ngForm" (submit)="addItemHandler(f)">
      <input type="text" ngModel name="name">
      <button type="submit">add</button>
    </form>

    <li *ngFor="let item of (items$ | async)">
      {{item.name}} - {{item.id}}
      <button (click)="detelItemHandler(item.id)">delete</button>
    </li>
  `,
})
export class ItemsComponent  {
  @ViewChild('f') form: NgForm;
  items$: Observable<Item[]> = this.store.select(getItems);
  getItemsError$: Observable<string> = this.store.select(getItemsError);
  error: boolean;

  constructor(private store: Store, actions$: Actions,
              @Inject(DOCUMENT) private document: Document) {
    // NOTE: invoked from item.effect when route is '/items'
    // this.store.dispatch(loadItems());

/*
    fromEvent(window, 'resize')
      .subscribe((e) => {
        console.log((e.target as any).innerWidth)
      });*/

    actions$
      .pipe(ofType(addItemSuccess))
      .subscribe(() => this.form.reset());
  }

  addItemHandler(f: NgForm): void {
    this.store.dispatch(addItem({ item: f.value }));

  }

  detelItemHandler(id: number): void {
    this.store.dispatch(deleteItem({ id }));
  }
}
