import { createAction, props } from '@ngrx/store';
import { Item } from '../../../model/item';

export const loadItems = createAction('[Items] load');
export const loadItemsSuccess = createAction('[Items] load success', props<{items: Item[]}>());
export const loadItemsFail = createAction('[Items] load fail');

export const addItem = createAction('[Items] Add', props<{ item: Item }>());
export const addItemSuccess = createAction('[Items] Add success', props<{ item: Item }>());
export const addItemFail = createAction('[Items] Add fail');

export const deleteItem = createAction('[Items] delete', props<{ id: number }>());
export const deleteItemSuccess = createAction('[Items] delete success', props<{ id: number }>());
export const deleteItemFail = createAction('[Items] delete fail', props<{ msg: string }>());

