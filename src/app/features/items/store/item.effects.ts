import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as ItemsActions from './items.actions';
import { catchError, concatMap, delay, filter, map, mergeMap, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { Item } from '../../../model/item';
import { of } from 'rxjs';
import { loadItems } from './items.actions';
import { createFeatureSelector, Store } from '@ngrx/store';
import { getSelectors, ROUTER_NAVIGATED, RouterReducerState } from '@ngrx/router-store';


// get router selectors
export const selectRouter = createFeatureSelector<RouterReducerState>('router');
export const { selectUrl } = getSelectors(selectRouter);


// ---------o----o----o--o-o-oO--------------->
@Injectable()
export class ItemEffects {

  goto$ = createEffect(() => this.actions$.pipe(
    ofType(ROUTER_NAVIGATED),
    withLatestFrom(this.store.select(selectUrl)),
    filter(([action, url]) => url === '/items'),
    // ofRoute('items'),
    map(() => loadItems())
  ));

  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(ItemsActions.loadItems),
    switchMap(
      () => this.http.get<Item[]>('http://localhost:3000/items')
        .pipe(
          map(items =>  ItemsActions.loadItemsSuccess({ items })),
          catchError(() => of(ItemsActions.loadItemsFail()))
        )
    )
  ));

  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(ItemsActions.deleteItem),
    mergeMap(
      ({ id }) => this.http.delete(`http://localhost:3000/items/${id}`)
        .pipe(
          delay(2000),
          map(() => ItemsActions.deleteItemSuccess({ id })),
          catchError((err: HttpErrorResponse) => of(ItemsActions.deleteItemFail({ msg: err.message })))
        )
    )
  ));

  addItem$ = createEffect(() => this.actions$.pipe(
    ofType(ItemsActions.addItem),
    concatMap(
      (action) => this.http.post<Item>(`http://localhost:3000/items`, action.item)
        .pipe(
          delay(200),
          map(item => ItemsActions.addItemSuccess({ item })),
          catchError(() => of(ItemsActions.addItemFail()))
        )
    )
  ));

  constructor(
    private store: Store,
    private http: HttpClient,
    private actions$: Actions
  ) { }
}
