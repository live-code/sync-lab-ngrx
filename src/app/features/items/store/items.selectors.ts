import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ProductsState } from '../items.module';
import { getSelectors, RouterReducerState } from '@ngrx/router-store';

// get router selectors
export const selectRouter = createFeatureSelector<RouterReducerState>('router');
export const { selectUrl } = getSelectors(selectRouter);

export const getProductsFeature = createFeatureSelector<ProductsState>('products');

export const getItems = createSelector(
  getProductsFeature,
  (state) => state.items.list
);

export const getItemsError = createSelector(
  getProductsFeature,
  state => state.items.error
);


/*
export const getItems2 = createSelector(
  getProductsFeature,
  selectUrl,
  (products, routerUrl) => products.items.list.filter
);
*/

