import { createReducer, on } from '@ngrx/store';
import { Item } from '../../../model/item';
import * as ItemsActions from './items.actions';

export interface ItemState {
  list: Item[];
  error: string;
}

const initialState: ItemState = {
  list: [],
  error: null
};

export const itemsReducer = createReducer(
  initialState,
  on(ItemsActions.loadItemsSuccess, (state, action) => ({ list: [...action.items], error: null })),
  on(ItemsActions.loadItemsFail, state => ({ ...state, error: 'load item fail' })),

  on(ItemsActions.addItemSuccess, (state, action) => ({list: [...state.list, action.item], error: null})),
  on(ItemsActions.addItemFail, state => ({ ...state, error: 'add fail' })),

  on(ItemsActions.deleteItem, state => ({ ...state, error: null })),
  on(ItemsActions.deleteItemSuccess, (state, action) => {
    return { ...state, list: state.list.filter(item => item.id !== action.id) };
  }),
  on(ItemsActions.deleteItemFail, (state, action) => ({ ...state, error: action.msg })),
);

/*

const initialState: Item[] = [];

export const itemsReducer = createReducer(
  initialState,
  on(ItemsActions.loadItemsSuccess, (state, action) => [...action.items]),
  on(ItemsActions.addItemSuccess, (state, action) => [...state, action.item]),
  on(ItemsActions.deleteItemSuccess, (state, action) => state.filter(item => item.id !== action.id)),
);
*/
