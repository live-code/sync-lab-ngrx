import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemsRoutingModule } from './items-routing.module';
import { ItemsComponent } from './items.component';
import { FormsModule } from '@angular/forms';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { itemsReducer, ItemState } from './store/items.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ItemEffects } from './store/item.effects';

export interface ProductsState {
  items: ItemState;
}
export const reducers: ActionReducerMap<ProductsState> = {
  items: itemsReducer
};

@NgModule({
  declarations: [ItemsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ItemsRoutingModule,
    StoreModule.forFeature('products', reducers),
    EffectsModule.forFeature([ItemEffects])
  ]
})
export class ItemsModule { }
