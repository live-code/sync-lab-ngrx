import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/store/auth/auth.guard';
import { AdminGuard } from './core/store/auth/admin.guard';

const routes: Routes = [
  { path: 'items', loadChildren: () => import('./features/items/items.module').then(m => m.ItemsModule) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'admin', canActivate: [AuthGuard], loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule) },
  { path: 'pexels', loadChildren: () => import('./features/pexels/pexels.module').then(m => m.PexelsModule) },
  { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
  { path: '**', redirectTo: 'login' }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
