import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { ItemEffects } from './features/items/store/item.effects';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { routerReducer, RouterReducerState, RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { RouterEffects } from './core/store/router/router.effects';
import { MiscEffects } from './core/store/misc/misc.effects';
import { authReducer, AuthState } from './core/store/auth/auth.reducer';
import { AuthEffects } from './core/store/auth/auth.effects';
import { AuthInterceptor } from './core/store/auth/auth.interceptor';
import { IfLoggedDirective } from './core/store/auth/if-logged.directive';

export interface AppState {
  authentication: AuthState;
  theme: { value: 'dark' | 'light' };
  router: RouterReducerState;
}

export const reducers: ActionReducerMap<AppState> = {
  authentication: authReducer,
  theme: () => ({ value: 'dark' }),
  router: routerReducer
};

@NgModule({
  declarations: [
    AppComponent,
    IfLoggedDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 20
    }),
    EffectsModule.forRoot([ AuthEffects, MiscEffects, RouterEffects ]),
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Minimal
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
