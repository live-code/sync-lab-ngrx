import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[slRow]'
})
export class RowDirective {
  @Input() slRow: 'xl' | 'md' | 'sm' = 'sm';
  @HostBinding() className = 'row';

}
