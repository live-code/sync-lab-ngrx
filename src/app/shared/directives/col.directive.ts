import { Directive, HostBinding, Input, Optional } from '@angular/core';
import { RowDirective } from './row.directive';

@Directive({
  selector: '[slCol]'
})
export class ColDirective {
  @Input() slCol: string | number;

  @HostBinding() get className(): string {
    const mq = this.parent?.slRow || 'sm';
    return `col-${mq}-${this.slCol}`;
  }

  constructor(
    @Optional() private parent: RowDirective
  ) {
    if (parent) {

    }
  }
}
