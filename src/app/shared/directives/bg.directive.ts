import { Directive, ElementRef, HostBinding, Input, OnChanges, OnInit, Renderer2, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[slBg]'
})
export class BgDirective implements OnChanges {
  @Input() slBg;
  @Input() fontSize;

  ngOnChanges(changes: SimpleChanges): void {

    const { slBg, fontSize } = changes;
    if (slBg) {
      this.renderer.setStyle(this.el.nativeElement, 'backgroundColor', slBg.currentValue);
    }

    if (fontSize) {
      this.renderer.setStyle(this.el.nativeElement, 'fontSize', `${fontSize.currentValue}px` );
    }
  }

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {
  }

  // solution with input set
  /*

  @Input() set slBg(value: string) {
    // this.el.nativeElement.style.backgroundColor = value;
    this.renderer.setStyle(this.el.nativeElement, 'backgroundColor', value);
  }
  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {}

  */


  // Solution with HOstBinding
  /*
  @HostBinding('style.backgroundColor') get bg(): string {
    return this.slBg;
  }

  @HostBinding('style.color') get color(): string {
    return this.slBg === 'green' ? 'white' : null;
  }
  */
  // @HostBinding() innerHTML = 'ciao';
  // @HostBinding() className = 'alert alert-info';
  // @HostBinding('style.fontSize') fontSize = '90px';


}

