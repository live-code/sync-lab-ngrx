import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[slUrl]'
})
export class UrlDirective {
  @Input() slUrl: string;
  @HostBinding('style.cursor') cursor = 'pointer';
  @HostBinding('style.textDecoration') deco = 'underline';
  @HostBinding('style.color') color = 'red';

  @HostListener('click')
  clickMe(): void {
    window.open(this.slUrl)
  }

}
