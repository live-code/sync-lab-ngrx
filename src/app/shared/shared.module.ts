import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BgDirective } from './directives/bg.directive';
import { UrlDirective } from './directives/url.directive';
import { RowDirective } from './directives/row.directive';
import { ColDirective } from './directives/col.directive';



@NgModule({
  declarations: [BgDirective, UrlDirective, RowDirective, ColDirective],
  imports: [
    CommonModule
  ],
  exports: [BgDirective, UrlDirective, RowDirective, ColDirective]
})
export class SharedModule { }
