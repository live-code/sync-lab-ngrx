import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './app.module';
import { Observable } from 'rxjs';
import { getDisplayName, getIsLogged } from './core/store/auth/auth.selectors';
import { logout } from './core/store/auth/auth.actions';

@Component({
  selector: 'sl-root',
  template: `
    <button routerLink="login">login</button>
    <button routerLink="admin" *ngIf="isLogged$ | async">admin</button>
    <button routerLink="items">products</button>
    <button routerLink="pexels">pexels</button>
    <button routerLink="uikit">uikit</button>
    
    <button (click)="logoutHandler()" *slIfLogged>logout</button>
    
    
    {{displayName$ | async}}
    <hr>
    <router-outlet></router-outlet>
    
  `,
})
export class AppComponent {
  displayName$: Observable<string> = this.store.select(getDisplayName);
  isLogged$: Observable<boolean> = this.store.select(getIsLogged);

  constructor(private store: Store<AppState>) { }

  logoutHandler(): void {
    this.store.dispatch(logout());
  }
}
